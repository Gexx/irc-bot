# IRC-Bot

### Description
Simple IRC bot. Coded for #hulk-ri ...

### Installation
You need to run:
```sh
$ npm install
```
Ok, now you are ready to go!
```sh
$ node app.js
```
Or
```sh
$ nohup node app.js &
$ disown -h %1
```

### Todos

 - Add more Code Comments
 - Fix bug where unexpectedly sometimes some function return "Cthulhu" ;)

License
----
MIT

**Free Software, Hell Yeah!**