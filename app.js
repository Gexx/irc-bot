// Load things we need
var irc 			= require('irc');
var request 	= require('request');
var jsonfile 	= require('jsonfile');
var logger 		= require('./logger.js');

// Configuration
var BOT = {
	nick: 'reGeXx_bot',
	version: '0.2.3',
	options: {
		userName: 'reGeXx_bot',
		password: "hehedatinebiitorekaoovojedrugipassworduproductionutrolololojesamte",
		secure: true,
		channels: ['#hulk-ri'],
		autoRejoin: false,
		autoConnect: true,
//	sasl: true,
//	debug: true,
//	showErrors: true,
		port: 5000
	}
};

logger.info('Starting up...');

// The most useless feature
var yourmomma = require("yourmomma");

//var client = new irc.Client('chat.freenode.net', BOT.nick, BOT.options);
var client = new irc.Client('znc.vretenar.pro', BOT.nick, BOT.options);

// Usefull thingy
function getRandomInt(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }

// EVENTS
client.addListener('join', function (channel, nick) {
    logger.info('User ' + nick + ' joined channel ' + channel);
});

client.addListener('message', function (from, to, message) {
	var parse, tag, quote, nick;
    logger.data('From: ' + from + ' => To: ' + to + ' | Message: ' + message);

    if( BOT.nick === to ){
		// Private message

		// Setting quotes
		if(message.match(/^\@\@/gi)){
			tag = '0';
			quote = '';
			if(message.match(/^\@\@\s*$/gi)){
				GuXBot.Quote(from, from, tag);
				return;
			}

			parse = message.match(/^\@\@\s*([0-9]{1})\s*$/);
			if(parse){
				if(typeof parse[1] === 'undefined'){ tag = '0'; }else{ tag = parse[1]; }
				GuXBot.Quote(from, from, tag);
				return;
			}

			parse = message.match(/^\@\@(\s([0-9]{1}))?(\s([a-zA-Z0-9\č\ć\š\đ\ž\_\-\!\"\'\@\(\)\[\]\&\$\.\,\s\:\;\\\/\<\>]{1,256}))?$/);
			if(parse){
				if(typeof parse[2] === 'undefined'){ tag = '0'; }else{ tag = parse[2]; }
				if(typeof parse[4] === 'undefined'){ quote = ''; }else{ quote = parse[4]; }
				GuXBot.AddQuote(from, from, tag, quote);
			}
		}else if(message.match(/^\!/gi)){

			if(message.match(/^\!help$/gi)){
				GuXBot.HelpMessage(from);
			}
			if(message.match(/^\!author$/gi)){
				GuXBot.AutorMessage(from);
			}
			if(message.match(/^\!quote/gi)){
				tag = '0';
				if(message.match(/^\!quote\s*$/gi)){
					GuXBot.Quote(from, from, tag);
					return;
				}

				parse = message.match(/^\!quote\s([a-zA-Z0-9\_\-\|]+)\s*([0-9]+)?/);
				if(parse){
					if(typeof parse[1] === 'undefined'){ nick = '0'; }else{ nick = parse[1];}
					if(typeof parse[2] === 'undefined'){ tag = '0'; }else{ tag = parse[2];}
					GuXBot.Quote(from, nick, tag);
				}
			}
			if(message.match(/^\!ls\-quote/gi)){
				if(message.match(/^\!ls\-quote\s*$/gi)){
					GuXBot.ListQuotes(from, from);
					return;
				}
				parse = message.match(/^\!ls\-quote\s([a-zA-Z0-9\_\-\|]+)\s*/);
				if(parse){
					if(typeof parse[1] === 'undefined'){ nick = '0'; }else{ nick = parse[1];}
					GuXBot.ListQuotes(from, nick);
				}
			}

			if(message.match(/^\!debug quote$/gi)){
				console.log(GuXBot.QuotesData);
			}
			if(message.match(/^\!save quote data$/gi)){
				GuXBot.SaveQuotes(from);
			}
			if(message.match(/^\!load quote data$/gi)){
				GuXBot.LoadQuotes(from);
			}

			if(message.match(/^\!xkcd/gi)){
				tag = getRandomInt(1, 1784).toString();

				if(message.match(/^\!xkcd\s*$/gi)){
					GuXBot.XKCD_Message(from, tag);
					return;
				}

				parse = message.match(/^\!xkcd\s+([0-9]{1,4})?/);
				if(parse){
					if(typeof parse[1] === 'undefined'){ tag = getRandomInt(1, 1784).toString(); }else{ tag = parse[1];}
					GuXBot.XKCD_Message(from, tag);
				}
			}

			if(message.match(/^\!joke$/gi)){
				GuXBot.JokeMessage(from);
			}
			if(message.match(/^\!mama$/gi)){
				GuXBot.MamaJokeMessage(from);
			}
		}else{
			GuXBot.WelcomeMessage(from);
		}

	}else{
		// Not private

		if(message.match(/^\@\@/gi)){
			tag = '0';
			quote = '';
			if(message.match(/^\@\@\s*$/gi)){
				GuXBot.Quote(to, from, tag);
				return;
			}

			parse = message.match(/^\@\@\s*([0-9]{1})\s*$/);
			if(parse){
				if(typeof parse[1] === 'undefined'){ tag = '0'; }else{ tag = parse[1]; }
				GuXBot.Quote(to, from, tag);
				return;
			}

			parse = message.match(/^\@\@(\s([0-9]{1}))?(\s([a-zA-Z0-9\č\ć\š\đ\ž\_\-\!\"\'\@\(\)\[\]\&\$\.\,\s\:\;\\\/\<\>]{1,256}))?$/);
			if(parse){
				if(typeof parse[2] === 'undefined'){ tag = '0'; }else{ tag = parse[2]; }
				if(typeof parse[4] === 'undefined'){ quote = ''; }else{ quote = parse[4]; }
				GuXBot.AddQuote(to, from, tag, quote);
			}
		}

		if(message.match(/^\!help$/gi)){
			GuXBot.HelpMessage(to);
		}
		if(message.match(/^\!author$/gi)){
			GuXBot.AutorMessage(to);
		}
		if(message.match(/^\!quote/gi)){
			tag = '0';
			if(message.match(/^\!quote\s*$/gi)){
				GuXBot.Quote(to, from, tag);
				return;
			}

			parse = message.match(/^\!quote\s([a-zA-Z0-9\_\-\|]+)\s*([0-9]+)?/);
			if(parse){
				if(typeof parse[1] === 'undefined'){ nick = '0'; }else{ nick = parse[1];}
				if(typeof parse[2] === 'undefined'){ tag = '0'; }else{ tag = parse[2];}
				GuXBot.Quote(to, nick, tag);
			}
		}
		if(message.match(/^\!ls\-quote/gi)){
			if(message.match(/^\!ls\-quote\s*$/gi)){
				GuXBot.ListQuotes(to, from);
				return;
			}
			parse = message.match(/^\!ls\-quote\s([a-zA-Z0-9\_\-\|]+)\s*/);
			if(parse){
				if(typeof parse[1] === 'undefined'){ nick = '0'; }else{ nick = parse[1];}
				GuXBot.ListQuotes(to, nick);
			}
		}

		if(message.match(/^\!xkcd/gi)){
			tag = getRandomInt(1, 1784).toString();

			if(message.match(/^\!xkcd\s*$/gi)){
				GuXBot.XKCD_Message(to, tag);
				return;
			}

			parse = message.match(/^\!xkcd\s+([0-9]{1,4})?/);
			if(parse){
				if(typeof parse[1] === 'undefined'){ tag = getRandomInt(1, 1784).toString(); }else{ tag = parse[1];}
				GuXBot.XKCD_Message(to, tag);
			}
		}

		if(message.match(/^\!joke$/gi)){
			GuXBot.JokeMessage(to);
		}
		if(message.match(/^\!mama$/gi)){
			GuXBot.MamaJokeMessage(to);
		}

		// RMS joke !
		if(message.match(/(gnu\/linux)|(kernel)|(gnu\splus\slinux)|(gnu\+linux)|(gnu\s\+\slinux)|(gnu\s+\/\s+linux)|(linux\s+\/\s+gnu)/gi)){
			// He said gnu/linux so it is good
			if(message.match(/(\s+linux\s+)/gi)){
				if(BOT.nick !== from){
					//GuXBot.RMSMessage(to, from);
				}
			}
		}else{
			if(message.match(/(linux)/gi)){
				if(BOT.nick !== from){
					GuXBot.RMSMessage(to, from);
				}
			}
		}

		// Other inside jokes !
			if(message.match(/(\s*who\s+will\s+pay\s+for\s*)/gi)){
				if(BOT.nick !== from){
					client.say(to, "MEXICO!");
				}
			}

			if(message.match(/(\s*who\s*is\s+going\s+to\s+pay\s+for\s*)/gi)){
				if(BOT.nick !== from){
					client.say(to, "MEXICO!");
				}
			}

			if(message.match(/(dtrace)/gi)){
				if(BOT.nick !== from){
					GuXBot.dtrace_Message(to, from);
				}
			}

			if(message.match(/(freebsd)/gi)){
				if(BOT.nick !== from){
					GuXBot.freebsd_Message(to, from);
				}
			}

			if(message.match(/(trump)/gi)){
				if(BOT.nick !== from){
					GuXBot.trump_Message(to, from);
				}
			}


    }
});

client.addListener('error', function(message) {
    console.log('error: ', message);
});

/*
client.addListener('pm', function (from, message) {
	console.log('PM!');
});
*/

var GuXBot = {

	QuotesData: {},

	WelcomeMessage: function(target){
		client.say(target, 'Bok, ja sam novi IRC bot! Isprobaj komandu !help za više informacija.');
	},

	HelpMessage: function(target){
		client.say(target, 'Moguće komande za sada su: !help, @@ [num_tag] <smart_quote>, !quote [author_nick] [tag 0-9], !ls-quote [author_nick], !xkcd [number], !joke, !mama, !author');
	},

	AutorMessage: function(target){
		client.say(target, 'Ovaj IRC bot napravio je (re)GeXx, ako ti se sviđa bot ili te potpuno nervira slobodno me časti kavom; 14qcVU2KaB92STXJJPYhR4R1XwhAHQVQm9');
	},

	AddQuote: function (target, author, tag, data){
		if(typeof this.QuotesData[author] === "undefined"){
			this.QuotesData[author] = {};
		}
		this.QuotesData[author][tag] = data;
		client.say(target, 'Vaša izreka uspješno je zabilježena! Od sad je možete pozvati sa @@');
		client.say(target, '"'+this.QuotesData[author][tag]+'" - '+author);
	},

	Quote: function(target, author, tag){
		author = author || '';
		tag = tag || '0';
		if(typeof this.QuotesData[author] === "undefined"){
			client.say(target, 'Autor nije još postavio ni jednu izreku.');
			return;
		}
		if(typeof this.QuotesData[author][tag] === "undefined"){
			client.say(target, 'Autor nije postavio izreku pod navedenim tagom.');
			return;
		}
		client.say(target, '"'+this.QuotesData[author][tag]+'" - '+author);
	},
	ListQuotes: function(target, author){
		author = author || '';
		if(typeof this.QuotesData[author] === "undefined"){
			client.say(target, 'Autor nije još postavio ni jednu izreku.');
			return;
		}
		for (var key in this.QuotesData[author]) {
			if (this.QuotesData[author].hasOwnProperty(key)) {
				client.say(target, '"'+this.QuotesData[author][key]+'" - '+author+' | Tag: '+key);
			}
		}
	},

	SaveQuotes: function(target){
		jsonfile.writeFile('./.data/quotes.json', this.QuotesData, {spaces: 2}, function(err) {
			if(err){ logger.error(err); return; }
			logger.info('Quotes data saved successfully!');
			client.say(target, 'All quotes saved!');
		});
	},

	LoadQuotes: function(target){
		jsonfile.readFile('./.data/quotes.json', function(err, obj) {
			if(err){ logger.error(err); return; }
			GuXBot.QuotesData = obj;
			logger.info('Quotes data loaded successfully!');
		});
	},

	JokeMessage: function(target){
		request({ url: 'http://api.icndb.com/jokes/random', 'json': true}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				client.say(target, body.value.joke);
			}
		});
		//client.say(target, '*Kuc*, *kuc*... ~tko je ? -Javascript  ~Javascript who ? undefined.');
	},

	MamaJokeMessage: function(target){
		client.say(target, yourmomma.generate());
	},

	RMSMessage: function(target, from){
		client.say(target, "I'd just like to interject for moment. "+from+" what you're refering to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux.");
	},

	XKCD_Message: function(target, number){
		client.say(target, "https://xkcd.com/"+number);
	},

  trump_Message: function(target, from){
    client.say(target, "Mexico will pay for my firewall!");
  },

  freebsd_Message: function(target, from){
    client.say(target, "♥ GNU/Linux ♥");
  },

  dtrace_Message: function(target, from){
    client.say(target, "*d(ick)trace");
  }

};

// Loading the quotes from data file...
jsonfile.readFile('./.data/quotes.json', function(err, obj) {
	if(err){ logger.error(err); return; }
	GuXBot.QuotesData = obj;
	logger.info('Quotes data loaded successfully!');
});
